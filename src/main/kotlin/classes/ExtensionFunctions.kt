package classes

fun Rocket.configShield(state:Boolean,number:Int){
    if (state){
        this.activeShield = true
        
        println("Sending rocket $number with shield, Weight = ${this.actualCargo}")
    }else{
        this.activeShield = false
        println("Rocket $number with shield has exploded!!, Weight = ${this.actualCargo}")
    }
}
