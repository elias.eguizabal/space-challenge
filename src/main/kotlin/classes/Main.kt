package classes

fun main(args:Array<String>){

    Main().simulation1()
    Main().simulation2()
    Main().simulation3()
    Main().simulation4()
}

class Main {

    private val simulation = Simulation()

    fun simulation1(){
        println("Starting simulation 1...")
        val itemsPhase1 = simulation.loadItems(1)
        val fleetU1Phase1 = simulation.loadU1(itemsPhase1)
        val budget1 = simulation.runSimulation(fleetU1Phase1)
        resumen(1,1,budget1,fleetU1Phase1)
    }

    fun simulation2(){
        println("Starting simulation 2...")
        val itemsPhase2 = simulation.loadItems(2)
        val fleetU1Phase2 = simulation.loadU1(itemsPhase2)
        val budget2 = simulation.runSimulation(fleetU1Phase2)
        resumen(1,2,budget2,fleetU1Phase2)
    }

    fun simulation3(){
        println("Starting simulation 3...")
        val itemsPhase1 = simulation.loadItems(1)
        val fleetU2Phase1 = simulation.loadU2(itemsPhase1)
        val budget3 = simulation.runSimulation(fleetU2Phase1)
        resumen(2,1,budget3,fleetU2Phase1)
    }

    fun simulation4(){
        println("Starting simulation 4...")
        val itemsPhase2 = simulation.loadItems(2)
        val fleetU2Phase2 = simulation.loadU2(itemsPhase2)
        val budget4 = simulation.runSimulation(fleetU2Phase2)
        resumen(2,2,budget4,fleetU2Phase2)
    }

    fun resumen(flota:Int,fase:Int,budget:Int,fleet:ArrayList<Rocket>){
        val enviados = budget/fleet[0].cost
        println("\nSimulation summary: Fleet $flota phase $fase\n" +
                "Rockets sent: $enviados\n" +
                "Crashed rockets: ${enviados-fleet.size}\n" +
                "Budget = $$budget Million \n")
    }

}