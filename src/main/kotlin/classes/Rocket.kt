package classes

import interfaces.SpaceShipInterface

open class Rocket(val cost:Int,val weight:Int,val maxWeight:Int) : SpaceShipInterface {

    var actualCargo = 0
    var maxCargo = maxWeight-weight
    var activeShield = false
    val actualItems = arrayListOf<Item>()

    override fun launch(): Boolean {
        return true
    }

    override fun land(): Boolean {
        return true
    }

    override fun canCarry(item: Item): Boolean {
        return actualCargo+item.weight <= maxCargo
    }

    override fun carry(item: Item) {
        actualCargo += item.weight
        actualItems.add(item)
    }
}