package classes

import java.io.File

class Simulation() {

    private val paht1 = "src/main/resources/phase-1.txt"
    private val paht2 = "src/main/resources/phase-2.txt"


    fun loadItems(phase: Int): ArrayList<Item> {
        val itemList = arrayListOf<Item>()
        when (phase) {
            1 -> {
                File(paht1).forEachLine {
                    itemList.add(Item(it.split("=")[0], it.split("=")[1].toInt()))
                }
            }
            2 -> {
                File(paht2).forEachLine {
                    itemList.add(Item(it.split("=")[0], it.split("=")[1].toInt()))
                }
            }
        }
        return itemList
    }

    fun loadU1(itemList: ArrayList<Item>): ArrayList<Rocket> {
        val rocketsU1List = arrayListOf<Rocket>()
        val remainTemp = arrayListOf<Item>()

        while (itemList.isNotEmpty()) {
            val u1Temp = U1()
            itemList.forEach {
                if (u1Temp.canCarry(it)) {
                    u1Temp.carry(it)
                } else {
                    remainTemp.add(it)
                }
            }

            rocketsU1List.add(u1Temp)
            itemList.clear()
            itemList.addAll(remainTemp)
            remainTemp.clear()
        }
        return rocketsU1List
    }

    fun loadU2(itemList: ArrayList<Item>): ArrayList<Rocket> {
        val rocketsU2List = arrayListOf<Rocket>()
        val remainTemp = arrayListOf<Item>()

        while (itemList.isNotEmpty()) {
            val u2Temp = U2()
            itemList.forEach {
                if (u2Temp.canCarry(it)) {
                    u2Temp.carry(it)
                } else {
                    remainTemp.add(it)
                }
            }

            rocketsU2List.add(u2Temp)
            itemList.clear()
            itemList.addAll(remainTemp)
            remainTemp.clear()
        }
        return rocketsU2List
    }

    fun runSimulation(rocketsList: ArrayList<Rocket>): Int {
        var totalBudget = 0
        var enviados = 0

        rocketsList.forEach {rocket ->
            var complete = false
            while (!complete) {
                enviados += 1

                if (enviados.rem(3) == 0) {
                    rocket.configShield(true,enviados)
                } else {
                    println("Sending rocket $enviados, Weight = ${rocket.actualCargo}")
                }

                if (rocket.launch() && rocket.land()) {
                    if (rocket.activeShield) {
                        println("Rocket with shield lands successfully")
                    }
                    complete = true
                } else {
                    if (rocket.activeShield) {
                        rocket.configShield(false,enviados)
                    }else{
                        println("Rocket has exploded!!")
                    }
                    println("Sending new rocket..")
                }

                totalBudget += rocket.cost
            }
        }

        return totalBudget
    }

}