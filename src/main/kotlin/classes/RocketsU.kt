package classes

import kotlin.random.Random

class U1(cost: Int = 100, weight: Int = 10000, maxCargo: Int = 18000) : Rocket(cost, weight, maxCargo) {

    override fun launch(): Boolean {
        return Random.nextInt(0, 100) > 5.times(actualCargo.div(maxCargo))
    }


    override fun land(): Boolean {
        return Random.nextInt(0, 100) > 1.times(actualCargo.div(maxCargo))
    }
}

class U2(cost: Int = 120, weight: Int = 18000, maxCargo: Int = 29000):Rocket(cost, weight, maxCargo) {


    override fun launch(): Boolean {
        return Random.nextInt(0, 100) > 4.times(actualCargo.div(maxCargo))
    }

    override fun land(): Boolean {
        return Random.nextInt(0, 100) > 8.times(actualCargo.div(maxCargo))
    }
}