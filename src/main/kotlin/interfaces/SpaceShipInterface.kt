package interfaces

import classes.Item

interface SpaceShipInterface {
    fun launch(): Boolean
    fun land(): Boolean
    fun canCarry(item: Item):Boolean
    fun carry(item: Item)
}